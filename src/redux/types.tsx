export const SET_USER: string = "SET_USER";
export const SET_AUTHENTICATED: string = "SET_AUTHENTICATED";
export const SET_UNAUTHENTICATED: string = "SET_UNAUTHENTICATED";
